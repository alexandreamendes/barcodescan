import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers :[
    BarcodeScanner
  ]
})
export class HomePage {

  constructor(public navCtrl: NavController,
              private barcodeScanner: BarcodeScanner) {

  }


  LerCodBar(){
    this.barcodeScanner.scan().then(barcodeData => {
      alert('Barcode data' + barcodeData);
     }).catch(err => {
      alert('Error'+ err);
     });
  }

}
